# README #

This is a java puzzle. I have no solution so far.

### What is the problem to solve? ###

* There is a warning during the compilation, which says: VehicleServiceImplementation.java uses unchecked or unsafe operations.
* Of course the problem can be solved with adding @SuppressWarning("unchecked") to the retrieve method, but this is not a solution, but a workaround

### How to solve it? ###

* Let's say that the interface comes from an external library, through a jar, so can't changed
* Find a way to avoid the warning, but still use a Map to store the possible values

### How to check it? ###

* simply build with maven: mvn clean install
* you will find the warning in the console
* you can try out the code works if you run it:
1. mvn clean install
2. go to target directory
3. java -jar avoid-suppress-warning-1.0-SNAPSHOT.jar

### How to share the ideas? ###
* Create a pull request
* Shoot a message
* Write a comment
* Any other way appreciated