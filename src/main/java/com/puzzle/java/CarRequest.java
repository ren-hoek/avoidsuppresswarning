package com.puzzle.java;

public class CarRequest {

    private Long id;

    public CarRequest(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
