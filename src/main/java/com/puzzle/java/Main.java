package com.puzzle.java;

/**
 * @author Sandor_Laczko
 */
public class Main {

    public static void main(String[] args) {
        VehicleServiceImplementation vehicleServiceImplementation = new VehicleServiceImplementation();
        VehicleResult<Car> vehicleResult = vehicleServiceImplementation.retrieve(new CarRequest(5L));
        System.out.println(vehicleResult);
    }
}
