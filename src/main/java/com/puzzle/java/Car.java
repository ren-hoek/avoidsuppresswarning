package com.puzzle.java;

public class Car {

    private Long id;

    public Car(final Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Car [" + "id=" + id + ']';
    }
}
