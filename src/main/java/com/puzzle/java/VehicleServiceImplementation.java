package com.puzzle.java;

import java.util.HashMap;
import java.util.Map;

public class VehicleServiceImplementation implements VehicleService {

    private static Map<Long, VehicleResult> resultMap = new HashMap<Long, VehicleResult>();

    static {
        resultMap.put(5L, new VehicleResult<Car>(new Car(5L)));
    }

    public <CAR extends Car> VehicleResult<CAR> retrieve(final CarRequest request) {
        return resultMap.get(request.getId());
    }
}
