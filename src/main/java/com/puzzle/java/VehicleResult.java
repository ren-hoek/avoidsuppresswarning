package com.puzzle.java;

public class VehicleResult<T extends Car> {

    private T vehicle;

    public VehicleResult(final T vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String toString() {
        return "VehicleResult [" + "vehicle=" + vehicle + ']';
    }
}
