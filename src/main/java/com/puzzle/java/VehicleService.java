package com.puzzle.java;

public interface VehicleService {

    <CAR extends Car> VehicleResult<CAR> retrieve(CarRequest request);
}
